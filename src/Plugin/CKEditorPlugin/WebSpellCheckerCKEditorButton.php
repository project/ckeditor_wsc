<?php

namespace Drupal\ckeditor_wsc\Plugin\CKEditorPlugin;

use Drupal\ckeditor\CKEditorPluginBase;
use Drupal\ckeditor\CKEditorPluginConfigurableInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\editor\Entity\Editor;

/**
 * Defines the "wsc" plugin.
 *
 * NOTE: The plugin ID ('id' key) corresponds to the CKEditor plugin name.
 * It is the first argument of the CKEDITOR.plugins.add() function in the
 * plugin.js file.
 *
 * @CKEditorPlugin(
 *   id = "wsc",
 *   label = @Translation("WebSpellChecker")
 * )
 */
class WebSpellCheckerCKEditorButton extends CKEditorPluginBase implements CKEditorPluginConfigurableInterface {

  /**
   * {@inheritdoc}
   */
  public function isInternal() {
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function getFile() {
    return base_path() . 'libraries/wsc/plugin.js';
  }

  /**
   * {@inheritdoc}
   *
   * NOTE: The keys of the returned array corresponds to the CKEditor button
   * names. They are the first argument of the editor.ui.addButton() or
   * editor.ui.addRichCombo() functions in the plugin.js file.
   */
  public function getButtons() {
    // Key must match precisely what it used for editor.ui.addButton in
    // plugin.js, and this is what is used.
    return [
      'SpellChecker' => [
        'label' => $this->t('WebSpellChecker'),
        'image' => base_path() . 'libraries/wsc/icons/spellchecker.png',
      ],
    ];
  }

  /**
   * Drupal language-code to WebSpellChecker.net specification mapper.
   *
   * Defaults to en_US. Regardless of this setting, the user's preference
   * in the WYSIWYG will be saved.
   *
   * @param string $lang_code
   *   The language code to get a mapping for.
   *
   * @return string|en_US
   *   The WebSpellChecker.net slang code.
   *
   * @see https://docs.webspellchecker.net/display/KnowledgeBase/Default+Languages+List
   * @see https://docs.webspellchecker.net/display/KnowledgeBase/Additional+Languages+List
   */
  public function lookupLanguage($lang_code) {

    $table = [
      'en' => 'en_US',
      'da' => 'da_DK',
      'nl' => 'nl_NL',
      'fi' => 'fi_FI',
      'fr' => 'fr_FR',
      'de' => 'de_DE',
      'el' => 'el_GR',
      'it' => 'it_IT',
      'nb' => 'nb_NO',
      'pt' => 'pt_PT',
      'es' => 'es_ES',
      'sv' => 'sv_SE',
    ];

    return isset($table[$lang_code]) ? $table[$lang_code] : 'en_US';
  }

  /**
   * {@inheritdoc}
   */
  public function getConfig(Editor $editor) {

    // Defaults.
    $config = [];
    $settings = $editor
      ->getSettings();
    if (isset($settings['plugins']['wsc'])) {
      $config = $settings['plugins']['wsc'];
    }

    // Empty means to use site default.
    if (empty($config['wsc_lang'])) {
      $lang_code = \Drupal::languageManager()->getDefaultLanguage()->getId();
      $config['wsc_lang'] = $this->lookupLanguage($lang_code);
    }

    return $config;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state, Editor $editor) {

    // Defaults.
    $config = [
      'wsc_customerId' => '',
      'wsc_customDictionaryIds' => '',
      'wsc_userDictionaryName' => '',
      'wsc_customLoaderScript' => '',
      'wsc_cmd' => 'spell',
      'wsc_width' => 580,
      'wsc_height' => FALSE,
      'wsc_left' => FALSE,
      'wsc_top' => FALSE,
    ];
    $settings = $editor
      ->getSettings();
    if (isset($settings['plugins']['wsc'])) {
      $config = $settings['plugins']['wsc'];
    }

    $form['wsc_customerId'] = [
      '#title' => $this
        ->t('Customer ID'),
      '#type' => 'textfield',
      '#default_value' => $config['wsc_customerId'],
      '#description' => $this->t('The %url% Service ID. It is used for hosted users only. It is required for migration from free to trial or paid versions.', [
        '%url%' => 'WebSpellChecker.com',
      ]),
    ];

    $form['wsc_customDictionaryIds'] = [
      '#title' => $this
        ->t('Custom Dictionary IDs'),
      '#type' => 'textfield',
      '#default_value' => $config['wsc_customDictionaryIds'],
      '#description' => $this->t("It links WSC to custom dictionaries. It should be a string with dictionary IDs separated by commas (','). Available only for the licensed version."),
    ];

    $form['wsc_userDictionaryName'] = [
      '#title' => $this
        ->t('User Dictionary ID'),
      '#type' => 'textfield',
      '#default_value' => $config['wsc_userDictionaryName'],
      '#description' => $this->t('It activates a user dictionary for WSC. The user dictionary name should be used. Available only for the licensed version.'),
    ];

    // TODO: Make this do some upload logic.
    $form['wsc_customLoaderScript'] = [
      '#title' => $this
        ->t('Custom Loader Script'),
      '#type' => 'textfield',
      '#default_value' => $config['wsc_customLoaderScript'],
      '#description' => $this->t('The parameter sets the URL to WSC file. It is required to the licensed version of WSC application.'),
    ];

    $form['wsc_cmd'] = [
      '#title' => $this
        ->t('Active Tab'),
      '#type' => 'select',
      '#default_value' => $config['wsc_cmd'],
      '#description' => $this->t('The parameter sets the active tab, when the WSC dialog is opened.'),
      '#options' => [
        'spell' => $this->t('Spell check'),
        'thes' => $this->t('Thesaurus'),
        'grammar' => $this->t('Grammar'),
      ],
    ];

    $form['wsc_width'] = [
      '#title' => $this
        ->t('Popup width (pixels)'),
      '#type' => 'textfield',
      '#default_value' => $config['wsc_width'],
      '#description' => $this->t('The parameter sets width of the WSC pop-up window. Specified in pixels.'),
    ];

    $form['wsc_height'] = [
      '#title' => $this
        ->t('Popup height (pixels)'),
      '#type' => 'textfield',
      '#default_value' => $config['wsc_height'],
      '#description' => $this->t('The parameter sets height of the WSC pop-up window. Specified in pixels.'),
    ];

    $form['wsc_left'] = [
      '#title' => $this
        ->t('Popup left margin (pixels)'),
      '#type' => 'textfield',
      '#default_value' => $config['wsc_left'],
      '#description' => $this->t('The parameter sets left margin of the WSC pop-up window. Specified in pixels.'),
    ];

    $form['wsc_top'] = [
      '#title' => $this
        ->t('Popup top margin (pixels)'),
      '#type' => 'textfield',
      '#default_value' => $config['wsc_top'],
      '#description' => $this->t('The parameter sets top margin of the WSC pop-up window. Specified in pixels.'),
    ];

    return $form;
  }

}
